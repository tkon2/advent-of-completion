<html>
  <head>
    <meta charset="utf-8">
    <style>
      body {
        padding: 10px;
        margin: 0;
        background-color: #0f0f23;
        color: #ccc;
        display: flex;
        flex-direction: column;
        gap: 50px;
        font-family: "Source Code Pro", monospace;
      }
      pre {
        padding: 0;
        margin: 0;
        font-family: "Source Code Pro", monospace;
      }
      .title {
        color: #00cc00;
        text-shadow: 0 0 2px #00cc00, 0 0 5px #00cc00;
      }
      .day-title {
        color: #00cc00;
        text-shadow: 0 0 2px #00cc00, 0 0 5px #00cc00;
      }
      .task1 {
        color: #9999cc;
        text-shadow: 0px 0px 4px #9999cc;
      }
      .task2 {
        color: gold;
        text-shadow: 0px 0px 4px gold;
      }
      .line {
        color: #444;
      }
      .line-bright {
        color: #666;
      }
      .last-updated {
        color: #666;
        font-size: 12px;
      }
    </style>
  </head>
  <body>
    <pre class="title">
    _       _                 _            __    ____                      _      _   _
   / \   __| |_   _____ _ __ | |_    ___  / _|  / ___|___  _ __ ___  _ __ | | ___| |_(_) ___  _ __
  / _ \ / _` \ \ / / _ \ '_ \| __|  / _ \| |_  | |   / _ \| '_ ` _ \| '_ \| |/ _ \ __| |/ _ \| '_ \
 / ___ \ (_| |\ V /  __/ | | | |_  | (_) |  _| | |__| (_) | | | | | | |_) | |  __/ |_| | (_) | | | |
/_/   \_\__,_| \_/ \___|_| |_|\__|  \___/|_|    \____\___/|_| |_| |_| .__/|_|\___|\__|_|\___/|_| |_|
                                                                    |_|
    </pre>
<?php

/**
 * Prints to STDERR
 *
 * @param string $str
 * @return void
 */
function clg($str) {
  fwrite(STDERR, $str . PHP_EOL);
}


function getPlayerTimeString($num) {
  $seconds = $num * 60;
  $str = (new DateTime("@$seconds"))->format('h:i:s');
  if ($num >= 60 * 24) {
    $days = floor($num / (60 * 24));
    $s = $days === 1 ? '' : 's';
    $str = $str . " +$days day$s";
  }
  return $str;
}

/**
 * Formats a datetime into a readable format
 *
 * @param DateTime $date
 * @return string
 */
function fd($date) {
  return $date->format('Y.m.d H:i:s');
}

/**
 * PrettyPrint, formats an object into a readable format
 *
 * @param mixed $var Variable to pretty print
 * @param boolean $nl NewLine - whether to include a newline at the end
 * @return void
 */
function pp($var, $nl = false) {
  print(json_encode($var, JSON_PRETTY_PRINT));
  if ($nl) print("\n");
}

/**
 * Actual string length, taking care of special characters
 *
 * @param string $str
 * @return int
 */
function slen($str) {
  $enc = mb_detect_encoding($str);
  return mb_strlen($str, "ASCII");
}

$json = json_decode(file_get_contents('./data.json'), true);

$players = $json['members'];

/**
 * Gets the difference between two dates in total minutes or seconds if includeSeconds is true
 *
 * @param DateTime $ref
 * @param int $timestamp
 * @param bool $includeSeconds
 * @return int Number of minutes
 */
function getDiffInMinutes($ref, $timestamp, $includeSeconds = false) {
  $diff = $ref->diff(new DateTime("@$timestamp"));
  $days = $diff->days * 24 * 60;
  $hours = $diff->h * 60;
  $minutes = $diff->i;
  if ($includeSeconds) {
    return ($days + $hours + $minutes) * 60 + $diff->s;
  }
  return $days + $hours + $minutes;
}

/**
 * Checks if given X coordinate is part of a line
 *
 * @param int $x
 * @return boolean
 */
function isLine($x) {
  if ($x < 100 && $x % 5 == 0) {
    return true;
  } else if ($x < 1000 && $x % 10 === 0) {
    return true;
  } else if ($x % 20 === 0) {
    return true;
  }
  return false;
}

/**
 * Prints the html for a given day
 *
 * @param mixed $players
 * @param int $year
 * @param int $day
 * @return void
 */
function getDay($players, $year, $day) {
  $start = new DateTime("$year-12-$day 00:00:00 GMT-05:00");
  $data = [];
  foreach($players as $id => $p) {
    $obj = [];
    $completion = $p['completion_day_level'];
    if (!array_key_exists($day, $completion)) continue;
    // pp($p);
    $obj['name'] = mb_convert_encoding($p['name'], 'ASCII');
    $obj['actualName'] = $p['name'];
    $task1Time = $completion[$day][1]['get_star_ts'];
    $obj['x1'] = getDiffInMinutes($start, $task1Time);
    $obj['t1'] = $task1Time;
    if (array_key_exists(2, $completion[$day])) {
      $task2Time = $completion[$day][2]['get_star_ts'];
      $obj['x2'] = getDiffInMinutes($start, $task2Time);
      $obj['t2'] = $task2Time;
    } else {
      $obj['x2'] = null;
      $obj['t2'] = null;
    }
    $data[] = $obj;
  }
  if (count($data) <= 0) return;

  usort($data, function ($a, $b) {
    if ($a['x1'] == $b['x1']) {
      return ($a['x2'] ?? INF) <=> ($b['x2'] ?? INF);
    }
    return $a['x1'] <=> $b['x1'];
  });

  $maxlen = 0;
  foreach($data as $d) {
    $s = max($d['x1'], $d['x2']);
    $nl = slen($d['name']);
    $maxlen = max($maxlen, $s+$nl+1);
  }
  $first = true;
  $daypostfix = str_repeat('-', 2-slen($day));
  $dayPost = $daypostfix . str_repeat('-', $maxlen - 7);

  $legend = "";
  $ignore = 0;
  for ($i = 1; $i <= $maxlen; $i++) {
    if (isLine($i)) {
      $legend .= "$i";
      $ignore = slen("$i") - 1;
    } else {
      if ($ignore > 0) {
        $ignore -= 1;
      } else {
        $legend .= ' ';
      }
    }
  }


  print("
    <div>
      <pre class=\"line-bright\">---<span class=\"day-title\">Day $day</span>$dayPost</pre>
      <pre class=\"line-bright\">$legend</pre>
");
  foreach($data as $d) {
    if ($first) {
      $first = false;
    } else {
      print("\n");
    }

    $x1 = $d['x1'];
    $prefix = str_repeat(" ", max(0, $x1 - 1));
    for ($i = 0; $i < slen($prefix); $i++) {
      if (isLine($i+1)) {
        $prefix[$i] = '|';
      }
    }
    $rest = '';
    $x2 = $d['x2'];
    if ($x2 != null) {
      $rest = str_repeat("-", max(0, $x2 - $x1 - 1));
      $rest .= 'O';
    }
    for ($i = 0; $i < slen($rest); $i++) {
      $index = slen($prefix) + 2 + $i;
      if (isLine($index)) {
        // $rest[$i] = '+';
      }
    }

    $name = $d['name'];
    $realName = $d['actualName'];
    $namePlaceholder = str_repeat('@', slen($name));
    $baseStr = "${prefix}o${rest} ${namePlaceholder}";

    $str = $baseStr . str_repeat(" ", max(0, $maxlen - slen($baseStr)));

    for ($i = slen($baseStr); $i < slen($str); $i++) {
      if (isLine($i+1)) {
        $str[$i] = '|';
      }
    }
    $t1stamp = $d['t1'] - (5 * 60 * 60);
    $t1 = getPlayerTimeString(getDiffInMinutes($start, $t1stamp, true));
    $t2 = '';
    if ($d['t2'] != null) {
      $t2stamp = $d['t2'] - (5 * 60 * 60);
      $t2 = getPlayerTimeString(getDiffInMinutes($start, $t2stamp, true));
    }

    $str = str_replace('|', "<span class=\"line\">|</span>", $str);

    $str = str_replace('O', "<span title=\"$t2\" class=\"task2\">2</span>", $str);
    $str = str_replace('o', "<span title=\"$t1\" class=\"task1\">1</span>", $str);
    $str = str_replace($namePlaceholder, $realName, $str);

    print("<pre>$str</pre>");
  }

  print('<pre class="line-bright">');
  print(str_repeat('-', $maxlen + 2));
  print('</pre>');

  print("
    </div>");



}

for ($i = 0; $i <= 25; $i += 1) {
  getDay($players, $json['event'], $i);
}

$timestamp = (new DateTime())->format('Y/m/d H:i:s');
print("<div class=\"last-updated\">Last updated $timestamp</div>");
?>

  </body>
</html>
